import React, { useState, useEffect } from "react";
import "../css/ChessBoard.css";
import ChessPiece from "./ChessPiece";

const ChessBoard = () => {
  const [currentPosition, setCurrentPosition] = useState({ x: 0, y: 0 });
  const [positionsVisited, setPositionsVisited] = useState([{ x: 0, y: 0 }]);
  const [isStartingPositionSet, setIsStartingPositionSet] = useState(false);
  const [isFinalised, setIsFinalised] = useState(false);
  const [intervalId, setIntervalId] = useState(null);


  const handleSquareClick = (x, y) => {
    if (!isStartingPositionSet) {
      setCurrentPosition({ x, y });
      setPositionsVisited([{ x, y }]);
      setIsStartingPositionSet(true);
    }
  };

  const moveChessPiece = () => {
    if (positionsVisited.length === 64) {
      console.log("All positions visited");
      return;
    }

    const possibleMoves = [
      { x: currentPosition.x + 2, y: currentPosition.y + 1 },
      { x: currentPosition.x + 2, y: currentPosition.y - 1 },
      { x: currentPosition.x - 2, y: currentPosition.y + 1 },
      { x: currentPosition.x - 2, y: currentPosition.y - 1 },
      { x: currentPosition.x + 1, y: currentPosition.y + 2 },
      { x: currentPosition.x + 1, y: currentPosition.y - 2 },
      { x: currentPosition.x - 1, y: currentPosition.y + 2 },
      { x: currentPosition.x - 1, y: currentPosition.y - 2 },
    ];
    for (let i = 0; i < possibleMoves.length; i++) {
      if (
        possibleMoves[i].x >= 0 &&
        possibleMoves[i].x <= 7 &&
        possibleMoves[i].y >= 0 &&
        possibleMoves[i].y <= 7 &&
        !positionsVisited.find(
          (pos) => pos.x === possibleMoves[i].x && pos.y === possibleMoves[i].y
        )
      ) {
        setCurrentPosition(possibleMoves[i]);
        setPositionsVisited([...positionsVisited, possibleMoves[i]]);
        setIsFinalised(true);
        break;
      }
    }
  };

  const reset = () => {
    setCurrentPosition({ x: 0, y: 0 });
    setPositionsVisited([{ x: 0, y: 0 }]);
    setIsStartingPositionSet(false);
    clearInterval(intervalId);
  }

  useEffect(() => {
    if (isStartingPositionSet) {
      setIntervalId(setInterval(moveChessPiece, 200));
    }
    return () => clearInterval(intervalId);
  }, [isStartingPositionSet, currentPosition, positionsVisited]);

  return (
    <>
      <table className="chess-board">
        <tbody>
          {Array(8)
            .fill()
            .map((_, i) => (
              <tr key={i}>
                {Array(8)
                  .fill()
                  .map((_, j) => {
                    const isBlackSquare = (i + j) % 2 === 1;
                    const squareColor = isBlackSquare ? "black" : "white";
                    const isVisited = positionsVisited.find(
                      (pos) => pos.x === i && pos.y === j
                    );
                    return (
                      <td
                        key={j}
                        className={`square ${squareColor} ${
                          isVisited ? "visited" : ""
                        }`}
                        onClick={() => handleSquareClick(i, j)}
                      >
                        {i === currentPosition.x &&
                        j === currentPosition.y ? (
                          <ChessPiece pieceType="king" color="black" />
                        ) : null}
                      </td>
                    );
                  })}
              </tr>
            ))}
        </tbody>
      </table>
      {isFinalised && (
        <div className="resetContainer">
          <h2>Do you want to reset?</h2>
          <p  onClick={() => reset() }> RESET</p>
        </div>
      )}
       <div className="finalisedPositions">
        {isFinalised ?(
          <p>List of positions covered:</p>
        ) : (<p>Choose any square you want!</p>)}
        <ul className="visited-positions">
          {positionsVisited.map((pos, index) => (
            <li key={index}>
              {`${String.fromCharCode(pos.y + 65)}${8 - pos.x}`}
            </li>
          ))}
        </ul>
      </div>
    </>
  );

};

export default ChessBoard;
