import React from 'react';
import "../css/ChessPiece.css";

const ChessPiece = ({ pieceType, color }) => {
  const pieceClass = `${color}-${pieceType}`;

  return <div className={`chess-piece ${pieceClass}`} />;
};

export default ChessPiece;