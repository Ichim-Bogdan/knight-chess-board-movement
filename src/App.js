import logo from './logo.svg';
import './App.css';
import ChessBoard from './components/ChessBoard';

function App() {
  return (
    <div className="App">
      <header className="App-header">
      <ChessBoard />
      </header>
    </div>
  );
}

export default App;
